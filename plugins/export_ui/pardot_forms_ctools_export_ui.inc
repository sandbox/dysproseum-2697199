<?php
/**
 * @file
 */

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'pardot_forms_fields',  // As defined in hook_schema().
  'access' => 'administer pardot',  // Define a permission users must have to access these pages.
  // Define the menu item.
  'menu' => array(
    'menu prefix' => 'admin/config/services',
    'menu item' => 'pardot_forms',
    'menu title' => 'Manage Pardot Forms',
    'menu description' => 'Administer Pardot Forms.',
  ),
  // Define user interface texts.
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('Pardot form'),
  'title plural proper' => t('Pardot forms'),
  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'pardot_forms_ctools_export_ui_form',
  ),
);

/**
 * Define the preset add/edit form.
 */
function pardot_forms_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];
  $form['form_type'] = array(
    '#type' => 'select',
    '#title' => t('Form type'),
    '#default_value' => $preset->form_type,
    '#required' => TRUE,
    '#options' => array(
      PARDOT_FORMS_SINGLE => 'Single-step',
      PARDOT_FORMS_MULTI => 'Multi-step',
    ),
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => 'Email field name',
    '#default_value' => $preset->email,
    '#required' => TRUE,
  );
  $form['step1_nid'] = array(
    '#type' => 'textfield',
    '#title' => 'Step 1 NID',
    '#default_value' => $preset->step1_nid,
    '#required' => FALSE,
  );
  $form['step1_field'] = array(
    '#type' => 'textfield',
    '#title' => 'Step 1 Field',
    '#default_value' => $preset->step1_field,
    '#required' => FALSE,
  );
  $form['step2_nid'] = array(
    '#type' => 'textfield',
    '#title' => 'Step 2 NID',
    '#default_value' => $preset->step2_nid,
    '#required' => FALSE,
  );
  $form['step2_field'] = array(
    '#type' => 'textfield',
    '#title' => 'Step 2 Field',
    '#default_value' => $preset->step2_field,
    '#required' => FALSE,
  );
  $form['step3_nid'] = array(
    '#type' => 'textfield',
    '#title' => 'Step 3 NID',
    '#default_value' => $preset->step3_nid,
    '#required' => FALSE,
  );
  $form['step3_field'] = array(
    '#type' => 'textfield',
    '#title' => 'Step 3 Field',
    '#default_value' => $preset->step3_field,
    '#required' => FALSE,
  );
  $form['step4_nid'] = array(
    '#type' => 'textfield',
    '#title' => 'Step 4 NID',
    '#default_value' => $preset->step4_nid,
    '#required' => FALSE,
  );
  $form['disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable this form?'),
    '#default_value' => $preset->disabled,
    '#required' => FALSE,
  );
}
