<?php
/**
 * @file
 * Admin forms.
 */

/**
 * Form constructor for pardot API settings.
 *
 * @ingroup forms
 */
function pardot_forms_admin_credentials() {
  $form = array();

  $form['pardot_forms_username'] = array(
    '#title' => t('Pardot API username'),
    '#type' => 'textfield',
    '#description' => t('E-mail address used to login to Pardot.'),
    '#default_value' => variable_get('pardot_forms_username', ''),
  );
  $form['pardot_forms_password'] = array(
    '#title' => t('Pardot API password'),
    '#type' => 'textfield',
    '#description' => t('Password used to login to Pardot.'),
    '#default_value' => variable_get('pardot_forms_password', ''),
  );
  $form['pardot_forms_user_key'] = array(
    '#title' => t('Pardot API user key'),
    '#type' => 'textfield',
    '#description' => t('API User Key found on <a href="@url">Pardot settings screen</a>.', array('@url' => 'https://pi.pardot.com/account')),
    '#default_value' => variable_get('pardot_forms_user_key', ''),
  );
  $form['pardot_forms_cookie_name'] = array(
    '#title' => t('Pardot account cookie name'),
    '#type' => 'textfield',
    '#description' => t('Name of cookie set by Pardot (ex. "visitor_id50332")'),
    '#default_value' => variable_get('pardot_forms_cookie_name', ''),
  );
  $form['pardot_forms_debug_mode'] = array(
    '#title' => t('Enable debug mode'),
    '#description' => t('Display additional debug messages'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('pardot_forms_debug_mode', FALSE),
  );

  return system_settings_form($form);
}
