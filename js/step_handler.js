(function ($) {
  "use strict";

  // Fetch correct form step and return to page.
  Drupal.behaviors.stepHandler = {
    attach: function (context, settings) {

    var $img = $('<img src="/sites/all/modules/contrib/colorbox/images/loading_animation.gif" />');

      // Run only on page load.
      if (context == document) {
        // Hide all form options initially.
        $('[class*="field-name-field-pardot-form-reference"] .webform-client-form', context).hide();
        $('.field-name-field-pardot-form-reference').append($img);

        var nid = settings.pardot_forms.currentNid;
        $.ajax(settings.basePath + 'pardot_forms/ajax/step/' + nid, {
          success: function(data) {
            // Now unhide the correct step form.
            if (data.webform_nid != null) {
              $img.hide();
              $("#webform-client-form-" + data.webform_nid, context).fadeIn();

              // Prefill fields.
              $.ajax(settings.basePath + 'pardot_forms/ajax/prefill/' + nid, {
                success: function(data) {
                  if (data != null) {
                    $(".field-name-field-pardot-form-reference input[type=email]", context).val(data.email);
                  }
                }
              });
            }
          }
        });
      }
    }
  }
}(jQuery));
