(function ($) {
  "use strict";

  Drupal.behaviors.prefillFields = {
    attach: function (context, settings) {

      var nid = settings.pardot_forms.currentNid;
      $.get('/pardot_forms/ajax/prefill/' + nid, function(data) {
        if (data != null) {
          $("input[type=email]").val(data.email);
        }
      });

    }
  }
}(jQuery));

