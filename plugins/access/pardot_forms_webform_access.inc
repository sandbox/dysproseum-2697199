<?php
/**
 * @file
 * Access plugin for pardot.
 */

/**
 * Plugins are described by creating a $plugin array which will
 * be used by the system that includes the file.
 */
$plugin = array(
  'title' => t('Pardot Forms Access: Multi-step webform'),
  'description' => t('Controls access by checking which form the user has completed.'),
  'settings form' => 'pardot_forms_access_settings_form',
  'callback' => 'pardot_forms_webform_check',
  'summary' => 'pardot_forms_webform_summary',
);

/**
 * Settings form for our access plugin.
 */
function pardot_forms_access_settings_form(&$form, &$form_state, $conf) {
  $form['settings']['step'] = array(
    '#type' => 'textfield',
    '#title' => t('Step Number'),
    '#description' => t('Determines which step in a multi-step form this pane corresponds to.'),
    '#default_value' => !empty($conf['step']) ? $conf['step'] : 1,
  );
  return $form;
}

/**
 * Custom callback defined by 'callback' in the $plugin array.
 *
 * Return FALSE to hide form, TRUE to show the form.
 */
function pardot_forms_webform_check($conf, $context) {

  $node = menu_get_object();
  $items = array();
  $items[1] = field_get_items('node', $node, 'field_pardot_form_reference');
  $items[2] = field_get_items('node', $node, 'field_pardot_form_reference_2');
  $items[3] = field_get_items('node', $node, 'field_pardot_form_reference_3');
  $items[4] = field_get_items('node', $node, 'field_pardot_form_reference_4');

  $nids = array();
  $nids[1] = $items[1][0]['target_id'];
  $nids[2] = $items[2][0]['target_id'];
  $nids[3] = $items[3][0]['target_id'];
  $nids[4] = $items[4][0]['target_id'];

  $nid = $nids[$conf['step']];

  // Check if previous steps were hidden.
  if ($conf['step'] > 1) {
    $next = pardot_forms_get_next_step($nids[$conf['step'] - 1]);
    if ($next == FALSE) {
    pardot_forms_status_message("Hiding step " . $conf['step']);
      return FALSE;
    }
  }

  // Check this current step.
  $next = pardot_forms_get_next_step($nid);
  if ($next == FALSE) {
    pardot_forms_status_message("Showing step " . $conf['step']);
    return TRUE;
  }
  else {
    pardot_forms_status_message("Hiding step " . $conf['step']);
    return FALSE;
  }

}

/**
 * Provide a summary.
 */
function pardot_forms_webform_summary($conf, $context) {
  return t('Pardot multi-step form');
}

