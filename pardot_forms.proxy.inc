<?php
/**
 * @file
 * Admin forms.
 */

/**
 * Form constructor for pardot API settings.
 *
 * @ingroup forms
 */
function pardot_forms_admin_proxy() {
  $form = array();

  $form['pardot_forms_proxy_server'] = array(
    '#title' => t('Proxy address'),
    '#type' => 'textfield',
    '#description' => t('IP address or hostname of proxy server.'),
    '#default_value' => variable_get('pardot_forms_proxy_server', ''),
  );
  $form['pardot_forms_proxy_port'] = array(
    '#title' => t('Proxy port'),
    '#type' => 'textfield',
    '#description' => t('Port number to connect to proxy server.'),
    '#default_value' => variable_get('pardot_forms_proxy_port', ''),
  );
  $form['pardot_forms_proxy_enable'] = array(
    '#title' => t('Enable proxy?'),
    '#description' => t('Use proxy settings to connect to Pardot API.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('pardot_forms_proxy_enable', FALSE),
  );

  return system_settings_form($form);
}
